import Layout from '../components/Layout.js'
import '../styles/globals.css'

export default function MyApp({ Component, pageProps }) {
  return (
      <>
    <Layout>
        <div className="container mx-auto">
          <Component {...pageProps} />
        </div>
    </Layout>
      </>
    )
  }