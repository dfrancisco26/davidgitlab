import Link from 'next/link';
import LinkBoxes from '../components/LinkBoxes';
import Head from 'next/head';
import { useState } from 'react';
import styles from '../styles/home.module.css';
import Typewriter from '../components/Typewriter';
import Player from '../components/Player';
import Skills from '../components/Skills';
import Banner from '../components/Banner';

export default function HomePage() {

  return (
    <div className={styles['container-div']}>
      <Head>
        <title>Main Page</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Banner />
        {/* <Player /> */}
        <Typewriter />
        <Skills />
        <LinkBoxes />     
      </main>
    </div>
  );
}
