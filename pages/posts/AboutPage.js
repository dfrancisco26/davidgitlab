import React from 'react';

function AboutPage() {

  return (
    <div className="container mx-auto">
      <div className="py-8">
        <h1>David Francisco</h1>
        <p className="text-gray-600 mb-4">Veteran and business owner turned software developer, dog dad, gamer, and soon-to-be husband.
          Learning new skills is my passion, and applying them to better the world is my mission. I thrive in creative, driven environments
          that prioritize human connection and communication. The discipline, leadership, and unerring commitment I gained in the Navy
          fuel everything I tackle. My stint as a business owner honed my people skills and gave me the room to truly figure out what I want
          from a career: constant growth, never-ending learning opportunities, and a chance to change the world for the better.</p>

        {/* Projects */}
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
          <div
            className="p-4 bg-white rounded-lg hover:shadow-lg transform hover:-translate-y-1 hover:scale-105 transition duration-300 ease-in-out"
          >
            {/* Project content */}
            {/* Replace the content below with your project details */}
            <h2 className="text-xl font-semibold mb-2">Project Title</h2>
            <p className="text-gray-600 mb-2">Project description goes here.</p>
            {/* ... */}
          </div>
          <div
            className="p-4 bg-white rounded-lg hover:shadow-lg transform hover:-translate-y-1 hover:scale-105 transition duration-300 ease-in-out"
          >
            {/* Project content */}
            {/* Replace the content below with your project details */}
            <h2 className="text-xl font-semibold mb-2">Project Title 2</h2>
            <p className="text-gray-600 mb-2">Project description goes here.</p>
            {/* ... */}
          </div>

          {/* Repeat the above div for each project */}
        </div>

        {/* Skills */}
        <div className="mb-4">{/* ... */}</div>

        {/* Images */}
        <div>{/* ... */}</div>
      </div>
    </div>
  );
}

export default AboutPage;
