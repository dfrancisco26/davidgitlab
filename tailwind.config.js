/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './app/**/*.js', // Note the addition of the `app` directory.
    './pages/**/*.js',
    './components/**/*.js',
    
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}