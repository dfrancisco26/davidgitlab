// components/Navbar.js
import Link from 'next/link';

const Navbar = () => {
  return (
    <nav className="p-4 bg-black border-b-4 border-indigo-600">
      <div className="container mx-auto flex justify-between items-center">
        <Link href="/HomePage">
          <a className="text-white text-2xl font-bold border-b-2 border-transparent hover:border-white transition">
            Homepage
          </a>
        </Link>
        <div className="space-x-4">
          <Link href="/posts/AboutPage">
            <a className="text-white text-lg border-b-2 border-transparent hover:border-white transition">
              About
            </a>
          </Link>
          <Link href="/posts/ProjectPage">
            <a className="text-white text-lg border-b-2 border-transparent hover:border-white transition">
              Projects
            </a>
          </Link>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
