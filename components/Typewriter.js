import React, { useEffect, useState } from 'react';
import styles from '../styles/typewriter.module.css';

const Typewriter = () => {
  const selfWords = ['creative', 'driven', 'honest', 'zealous', 'bold', 'eccentric', 'rational', 'observant', 'grounded', 'articulate', 'curious', 'passionate', 'loyal', 'dedicated', 'adaptable', 'resilient', 'empathetic', 'kind', 'honorable', 'studious', 'listening', 'funny', 'intelligent', 'thoughtful', 'hard-working', 'motivated', 'ambitious', 'caring', 'helpful', 'friendly', 'trustworthy', 'dependable', 'confident', 'patient', 'organized', 'flexible', 'open-minded', 'respectful', 'responsible', 'flexible', 'approachable', 'communicative', 'collaborative', 'cooperative', 'creative', 'curious', 'dedicated', 'dependable', 'determined', 'disciplined', 'efficient', 'energetic', 'enthusiastic', 'focused', 'friendly', 'generous', 'helpful', 'honest', 'imaginative', 'independent', 'innovative', 'kind', 'knowledgeable', 'loyal', 'motivated', 'organized', 'passionate', 'patient', 'persistent', 'positive', 'practical', 'productive', 'punctual', 'reliable', 'resourceful', 'responsible', 'self-disciplined', 'sensible', 'sincere', 'sociable', 'supportive', 'thoughtful', 'trustworthy', 'versatile', 'well-educated', 'well-mannered', 'well-rounded'];
  const techWords = ['Django', 'Python', 'HTML5', 'CSS3', 'Next.js', 'React', 'JavaScript', 'Node.js', 'Express.js', 'SQL', 'PostgreSQL', 'Git', 'GitHub', 'Heroku', 'Netlify', 'Bootstrap', 'Tailwind CSS', 'APIs', 'JSON', 'REST']
  const [index, setIndex] = useState(0);
  const [subIndex, setSubIndex] = useState(0);
  const [reverse, setReverse] = useState(false);
  
  const getRandomIndex = (currentIdx, arrayLength) => {
    let newIndex = Math.floor(Math.random() * arrayLength);
    while(newIndex === currentIdx) {
      newIndex = Math.floor(Math.random() * arrayLength);
    }
    return newIndex;
  }

  useEffect(() => {
    if (index === selfWords.length) return;
    
    if ( subIndex === selfWords[index].length + 1 && !reverse ) {
      setReverse(true);
      return;
    }

    if (subIndex === 0 && reverse) {
      setReverse(false);
      setIndex((prevIndex) => getRandomIndex(prevIndex, selfWords.length));
      return;
    }

    const timeout = setTimeout(() => {
      setSubIndex((prevSubIndex) => prevSubIndex + (reverse ? -1 : 1));
    }, 150);
    
    return () => clearTimeout(timeout);
  }, [subIndex, index, reverse]);

  return (
    <>
      <h2 className={styles['message-element']}>Always {`${selfWords[index].substring(0, subIndex) || "\u00A0"}`}</h2>
    </>
  );
}

export default Typewriter;
