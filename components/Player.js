import styles from '../styles/player.module.css';

export default function Player() {
  return (
    <div className={styles['player-div']}>
    <audio controls>
      <source src="/11-Dive.mp3" type="audio/mpeg" />
      Your browser does not support the audio element.
      </audio>
    </div>
  );
}