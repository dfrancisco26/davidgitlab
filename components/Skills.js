import React from 'react';
import styles from '../styles/skills.module.css';

export default function Skills() {
    const skills = ['Python', 'JavaScript', 'React', 'Node.js', 'CSS', 'HTML5', 'Git', 'Express.js', 'SQL', 'PostgreSQL', 'Bootstrap', 'Tailwind CSS', 'JSON', 'RESTful APIs', 'Django', 'Next.js', 'Heroku', 'Netlify', 'GitHub', 'Supabase', 'Firebase', 'Vercel', 'Gitlab', 'pytest', 'fastAPI', 'Docker'];

    return (
        <div className={`${styles['skills-section']} `}>
            <h2 className={styles['skills-title']}>Skills</h2>
            <ul className={styles['skills-list']}>
                {skills.map((skill, index) => (
                    <li key={index} className={styles['skill-item']}>{skill}</li>
                ))}
            </ul>
        </div>
    );
}