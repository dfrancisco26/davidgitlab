import Image from 'next/image';
import styles from '../styles/linkboxes.module.css';
    
export default function LinkBoxes() {
    return (
        <section className={styles['link-boxes-section']}>
            <a className={styles['linkedin']} target="_blank" rel="noreferrer" href="https://www.linkedin.com/in/davidn0tdave">
                <Image className={styles['linkedin-logo']} src='/images/linkedin.png' alt="Logo link to David's LinkedIn" width={50} height={50} />
            </a>
            <a className={styles['github']} target="_blank" rel="noreferrer" href="https://github.com/dfrancisco26">
                <Image className={styles['github-logo']} src='/images/github.png' alt="Logo link to David's Github" width={50} height={50} />
            </a>
            <a className={styles['gitlab']} target="_blank" rel="noreferrer" href="https://gitlab.com/dfrancisco26" >
            <Image className={styles['gitlab-logo']} src='/images/gitlab-logo.png' alt="Logo link to David's Gitlab" width={75} height={75} />
            </a>
        </section>
    );
}