import styles from '../styles/banner.module.css'

export default function Banner() {
    return (
        <div className='name-banner'>
          <h1 className={styles['name-banner-text']}>David, not Dave</h1>
        </div>
    )
}