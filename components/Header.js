import Link from 'next/link';


export default function Header() {
    return (
        <header className="sidebar-nav">
            <Link href="/">Back to home</Link>
            <Link href="/posts/AboutPage/">About</Link>
            <Link href="/posts/ProjectsPage/">Projects</Link>
        </header>
    );
}